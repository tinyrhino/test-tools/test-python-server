"""
Super simple server to receive and discard requests

Require python 3.x

"""

import http.server
import socketserver
import sys


try:
    PORT = sys.argv[1]
except:
    PORT = 8000

ADDRESS = ""

Handler = http.server.SimpleHTTPRequestHandler

try:
    # Threaded server
    server_class = http.server.ThreadingHTTPServer
except AttributeError:
    # Fallback for Python <3.7

    # Simple server
    server_class = http.server.HTTPServer

def run(server_class=server_class, handler_class=Handler):
    print(f"listening on {ADDRESS} port {PORT}")
    server_address = (ADDRESS, int(PORT))
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()
run()
