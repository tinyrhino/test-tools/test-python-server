"""
Credit to
https://medium.com/@peter.jp.xie/scale-up-rest-api-functional-tests-to-performance-tests-in-python-3239859c0e27

Run with
python3 tester.py

Optional arguments
--port: Overwrite the port
--address: Overwrite the address
--users: Define how many concurrent users
--loops: How many times should each user repeat the request
--url: Overwrite the default address (http://ADDRESS:PORT) with a custom

Defaults:
 * PORT NUMBER: 8000
 * ADDRESS: 127.0.0.1
 * CONCURRENT_USERS: 20
 * LOOP_TIMES: 30

"""
import argparse
import queue
from requests.adapters import HTTPAdapter
from requests import Session, exceptions
import sys
import threading
import time
from urllib3.util.retry import Retry



ADDRESS = "127.0.0.1"
PORT = 8000
URL = f'http://{ADDRESS}:{PORT}'

CONCURRENT_USERS = 20
LOOP_TIMES = 30

MAX_RETRIES = 20

queue_results = queue.Queue()
start_time = 0


def test_mock_service():
    # Prep the connection
    # More info on session and retry http://www.coglib.com/~icordasc/blog/2014/12/retries-in-requests.html
    s = Session()
    retries = Retry(total=MAX_RETRIES,
                    backoff_factor=0.1,
                    status_forcelist=[500, 502, 503, 504])
    s.mount('http://', HTTPAdapter(max_retries=retries))
    s.mount('https://', HTTPAdapter(max_retries=retries))


    resp = s.get(URL)
    if resp.status_code != 200:
        print('Test failed with response status code %s.' % resp.status_code )
        return 'fail', resp.elapsed.total_seconds()
    else:
        return 'pass', resp.elapsed.total_seconds()


def loop_test(loop_wait=0, loop_times=sys.maxsize):
    looped_times = 0
    while looped_times < LOOP_TIMES:
        # run an API test
        test_result, elapsed_time = test_mock_service()
        # put results into a queue for statistics
        queue_results.put(['test_mock_service', test_result, elapsed_time])

        # You can add more API tests in a loop here.
        looped_times += 1
        time.sleep(loop_wait)


def config():
    parser = argparse.ArgumentParser()
    # ENVIRONMENT
    parser.add_argument('--url', required=False, type=str)
    parser.add_argument('--port', required=False, type=str)
    parser.add_argument('--address', required=False, type=str)
    parser.add_argument('--users', required=False, type=str)
    parser.add_argument('--loops', required=False, type=str)
    return parser.parse_args()


if __name__ == '__main__':
    args = config()

    PORT = int(args.port) if args.port else PORT
    ADDRESS = args.address if args.address else ADDRESS
    URL = args.url if args.url else f'http://{ADDRESS}:{PORT}'
    LOOP_TIMES = int(args.loops) if args.loops else LOOP_TIMES
    CONCURRENT_USERS = int(args.users) if args.users else CONCURRENT_USERS


    print(f"URL: {URL}")
    print(f"CONCURRENT_USERS: {CONCURRENT_USERS}")
    print(f"LOOP_TIMES: {LOOP_TIMES}")




    workers = []
    start_time = time.time()
    print('Tests started at %s.' % start_time )

    # start concurrent user threads
    for i in range(CONCURRENT_USERS):
        thread = threading.Thread(target=loop_test, kwargs={'loop_times': LOOP_TIMES}, daemon=True)
        thread.start()
        workers.append(thread)

    # Block until all threads finish.
    for w in workers:
        w.join()

    end_time = time.time()
    print('\nTests ended at %s.' % end_time )
    print('Total test time: %s seconds.' %  (end_time - start_time) )
