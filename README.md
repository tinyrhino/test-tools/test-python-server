

# Supervisord

Install Supervisord to have the script running at boot time
```
sudo apt update
sudo apt install supervisor

sudo cp supervisord/test-server.conf /etc/supervisor/conf.d/
sudo supervisorctl reread
sudo supervisorctl update
```

If everything goes well you should now have 4 servers running waiting for requests on port 8000, 8001, 8003, 80004


# Nginx
Make the servers available via nginx

```
sudo apt update
sudo apt install nginx

sudo cp nginx/test-server.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/test-server.conf /etc/nginx/sites-enabled/
sudo nginx -t
sudo nginx restart
```
